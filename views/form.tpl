<script src="{{site_root}}/static/validator.min.js"></script>

<form id="comform" enctype="application/x-www-form-urlencoded" action="{{site_root}}/add" method="post" class="form-horizontal" role="form" data-toggle="validator">

<div class="form-group">
	<label class="col-sm-2 control-label">
		Ditt namn:
	</label>
	<div class="col-sm-6">
		<input class="form-control" type="text" name="author" id="author" required data-minlength="2" data-error="Skriv vad du heter!">
	</div>
	<div class="col-sm-offset-2 help-block with-errors"></div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">
		Kommentar:
	</label>
	<div class="col-sm-10">
		<textarea class="form-control" type="text" name="content" id="content" required data-minlength="3" data-error="Du måste ju skriva nåt här"></textarea>
	</div>
		<div class="col-sm-offset-2 help-block with-errors"></div>
</div>


<div class="form-group">
	<label class="col-sm-2 control-label">
		Article ref:
	</label>
	<div class="col-sm-10">
		<input class="form-control" type="text" name="article" id="article"     >
	</div>
</div>

<input type="submit" class="btn btn-success btn-medium">
<input type="button" onclick="refresh()" value="ladda om">
<input type="button" onclick="rerender()" value="rendera om">

<div id="pycomments"></div>

<script>
    var servertime_diff = 0;
    var comments_data = [];

    $(document).ready(function() {
        clearForm();
        $('#author').val( localStorage.getItem('author') );
    });

    function relative_custom(value, unit, ms, loc) {
          console.log('cust fn '+value+' '+unit+' '+ms);
          if(ms.abs() > (12).hour() && value == 1 && unit == 4) {
            return 'i går';
          }
          if (ms.abs() < (1).minute()) {
            return 'nyss';
          }
    }

    function get(str) {
        $.ajax( {url: '{{ site_root }}/static/'+str+'.json', cache:false}).success(function(data, stat, jqxhr){
            var localtime = Date.now();
            var servtime = Date.parse(jqxhr.getResponseHeader('Date'));
            servertime_diff = localtime - servtime;
            comments_data = data;
            rerender();
        }).error(function(x, str, y){
            console.log('ajax error '+str);
        })
	}

	//get comments from remote
    function refresh() {
        $('#pycomments').html('');
        get( $('#article').val() );
    }

    //just update the html (for time lapsed)
    function rerender() {
        $('#pycomments').html('');
         var el = $('#pycomments');
            comments_data.forEach(function(val) {
                if (val.author) {
                    el.append('<span>'+Date.create(Math.min(Date.now(), (val.timestamp*1000)+servertime_diff)).relative(relative_custom, 'sv')+', '+val.author+': '+ val.content+' </span><br>');
                }
            });
    }

    function clearForm() {
        $('#content').val('');
    }

	$('#comform').validator().on('submit', function(e) {
	    if (e.isDefaultPrevented())
	        return;
	    var data = new FormData();
	    data.append('author', $('#author').val());
        data.append('content', $('#content').val());
        data.append('article', $('#article').val());
        $.ajax({
          url: '{{ site_root }}/add',
          data: data,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(resdata){
            console.log('success');
            localStorage.setItem('author', data.get('author'));
            clearForm();
            refresh();
          },
          error: function(e) {
            console.log('error');
          }
        });
        e.preventDefault();
	});
</script>

<div>
	<p><br></p>
</div>
</div>
</form>

%rebase base
