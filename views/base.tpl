<!DOCTYPE html>
<html dir="ltr">
    <head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta charset="UTF-8">

		<link rel="stylesheet" href="{{site_root}}/static/bootstrap/css/bootstrap.min.css">

		<link rel="stylesheet" href="{{site_root}}/static/bootstrap/css/bootstrap-theme.min.css">

		<script type="text/javascript" src="{{site_root}}/static/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="{{site_root}}/static/sugar.min.js"></script>
		<script type="text/javascript" src="{{site_root}}/static/sugar.sv.js"></script>

		<script src="{{site_root}}/static/bootstrap/js/bootstrap.min.js"></script>

        <title>My Site!</title>
    </head>
    <body>
        <div id="pagebody" class="container">
            %include
        </div>
    </body>
</html>

