#!/usr/bin/python3
'''
Comment server side python bottle
'''
import logging
import bottle
import os
import sys
import re
import json
import time
import traceback
import threading
import glob
from bottle_log import LoggingPlugin

from bottle import template, request, redirect, BaseTemplate, Jinja2Template, jinja2_template

save_path_json = 'static/'
save_path = 'data/'
site_root = '/comments'
BaseTemplate.defaults['site_root'] = site_root
Jinja2Template.defaults['site_root'] = site_root

app = application = bottle.Bottle()
app.install(LoggingPlugin(app.config))
bottle.debug(True)

def generate_json_index(folder):
    lock = threading.Lock()
    lock.acquire(True)
    filenames = sorted(glob.glob(save_path+folder+'/*.json'))

    if not os.path.exists(save_path_json):
        os.makedirs(save_path_json)

    with open(save_path_json+folder+'.json', 'w') as outfile:
        outfile.write('[\n')
        for fname in filenames:
            with open(fname, 'r') as readfile:
                infile = readfile.read()
                for line in infile:
                    outfile.write(line)
                readfile.close()
                outfile.write(',\n')
        outfile.write('{}\n]');
        outfile.close()
    lock.release()

def generate_latest_comments_index(new_latest):
    lock = threading.Lock()
    lock.acquire(True)
    print('generate_latest')
    #print(new_latest)
    if not os.path.exists(save_path_json):
        os.makedirs(save_path_json)
    oldlist = []
    try:
        with open(save_path_json+'latest.json', 'r') as infile:
            oldlist = json.load(infile)
            print('old file: '+len(oldlist))
            infile.close()
    except:
        pass

    oldlist.append(new_latest)
    oldlist = sorted(oldlist, key=lambda comment: comment['timestamp'], reverse=True)
    if oldlist and len(oldlist) > 5:
        oldlist = oldlist[0:5]
    with open(save_path_json+'latest.json', 'w') as outfile:
        json.dump(oldlist, outfile)
        print('new file: '+str(len(oldlist)))
        outfile.close()
    lock.release()


@app.route('/static/<filename:path>')
def static(filename):
    '''
    Serve static files (served by nginx in 'production')
    '''
    return bottle.static_file(filename, root='./static')


@app.route('/')
def show_index():
    '''
    The front "index" page
    '''
    return 'Hello'


@app.route('/add', method='POST')
def do_add():
    comment = {}
    comment['author'] = request.forms.getunicode('author')
    comment['content'] = request.forms.get('content')
    comment['article'] = request.forms.get('article')
    comment['article_title'] = request.forms.get('article_title')
    comment['timestamp'] = time.time()
    print("/add" )
    if comment['author'] and comment['content'] and comment['article']:
        try:
            if not os.path.exists(save_path+comment['article']):
                os.makedirs(save_path+comment['article'])

            f = open('%s/%s_%s.json'%(save_path+comment['article'], str(comment['timestamp']), request.remote_addr ), 'w')
            json.dump(comment, f)
            f.close()
            generate_json_index(comment['article']) #blocking bad
            generate_latest_comments_index(comment)
        except:
            print('file open error')
            bottle.abort(500, 'file open error\n' + traceback.format_exc())
    else:
        print('err missing data')
        bottle.abort(500, 'err missing data')
    return 'ok'

@app.route('/comment/<threadid>', method='GET')
def do_comment(threadid):
    if os.path.exists(save_path_json + threadid + '.json'):
        return bottle.static_file(threadid + '.json', root=save_path_json)
    else:
        bottle.abort(404, 'no comments for '+threadid)

@app.route('/list', method='GET')
def do_list():
    files = glob.glob(save_path_json+'*.json')
    files2 = []
    if files:
        for f in files:
            art_name = re.sub(r'.*?/(.*?)\.json$',r'\1',f)
            comments = sorted(glob.glob(save_path+art_name+"/*.json"))
            if not comments:
                continue
            files2.append( {'name': art_name, 'num_comments': len(comments), 'latest': re.sub(r'.*/(\d+).*\.json$',r'\1',comments[-1])} )
            files2 = sorted(files2, key=lambda file: file['latest'], reverse=True)
        return jinja2_template('list.html', files=files2)
    else:
        bottle.abort(404, 'no comments')

@app.route('/latest', method='GET')
def do_latest():
    with open(save_path_json+'latest.json', 'r') as infile:
        comments = json.load(infile)
        if comments and len(comments) > 0:
            return jinja2_template('latest.html', comments=comments)
    bottle.abort(404, 'no comment')


@app.route('<filename:path>')
def show_generic(filename):
    if (filename.lower().endswith("html")):
        return jinja2_template(filename)
    return template(filename.lstrip('/'))


class StripPathMiddleware(object):
    '''
    Get that slash out of the request
    '''
    def __init__(self, a):
        self.a = a

    def __call__(self, e, h):
        e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self.a(e, h)


if __name__ == '__main__':
    #site_root = ''
    BaseTemplate.defaults['site_root'] = site_root

    bottle.run(
        app=StripPathMiddleware(app),
        host='0.0.0.0',
        port=8080,
        debug=True
    )
